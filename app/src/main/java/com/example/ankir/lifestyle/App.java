package com.example.ankir.lifestyle;

import android.app.Application;
import android.content.Context;

/**
 * Created by ankir on 27.06.2017.
 */

public class App extends Application {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }
}