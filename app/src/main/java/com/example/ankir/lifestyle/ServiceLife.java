package com.example.ankir.lifestyle;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;


/**
 * Created by ankir on 27.06.2017.
 */

public class ServiceLife extends Service implements SensorEventListener {
    SensorManager sensorManager;
    double rezult = 0; //
    long countRezult = 1;

    double maxDelta = 0;
    int countCurrent = 0;

    int green;
    int red;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();

        Observable.interval(1, TimeUnit.MINUTES).
                subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
                        sensorManager.registerListener(ServiceLife.this,
                                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                SensorManager.SENSOR_DELAY_NORMAL);
                    }
                });


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double abs = Math.sqrt(event.values[0] * event.values[0]
                + event.values[1] * event.values[1]
                + event.values[2] * event.values[2]);

        if (countCurrent < 60) { // обработка данных примерно за 1 сек
            if (abs < 11 && maxDelta < 11 - abs) {
                maxDelta = 11 - abs;  // максимальное отклонение от состояния покоя за минуту
            }
            countCurrent++;
        } else {
            sensorManager.unregisterListener(ServiceLife.this); // отключаем акселерометр
            rezult = (rezult + maxDelta); // если разделить на количество считываний - получим
            // среднее отклонение от состояния покоя - по факту активность
            green = (int) (rezult / countRezult * 100 / 11);
            red = 100 - green;
            Intent intent = new Intent(MainActivity.BROADCAST_ACTION);
            intent.putExtra("key", green);
            sendBroadcast(intent);

            //      Log.d("==========Green / Red  ", "Green = "+green + "%  Red =  " + red+"%");
            countRezult++;
            maxDelta = 0;
            countCurrent = 0;

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
