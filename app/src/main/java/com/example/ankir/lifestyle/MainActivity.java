package com.example.ankir.lifestyle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView colorView;
    TextView colorGreen;
    TextView colorRed;
    BroadcastReceiver broadcastReceiver;
    public static final String BROADCAST_ACTION = "com.example.ankir.lifestyle";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colorView = (TextView) findViewById(R.id.color_view);
        colorGreen = (TextView) findViewById(R.id.viev_green);
        colorRed = (TextView) findViewById(R.id.viev_red);


        findViewById(R.id.action_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ServiceLife.class);
                startService(intent);

            }
        });

        findViewById(R.id.action_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ServiceLife.class);
                stopService(intent);
            }
        });


        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int green = intent.getIntExtra("key", 0);
                int red = 100 - green;
                Log.d("==========Green / Red  ", "Green = " + green + "%  Red =  " + red + "%");
                colorView.setBackgroundColor(Color.rgb(red * 255 / 100, green * 255 / 100, 0));
                colorGreen.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, (float) green / 100));
                colorRed.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, (float) red / 100));
                colorGreen.setText(green + "%");
                colorRed.setText(red + "%");

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

}
